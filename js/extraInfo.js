/*
	1) Buscar o json e guardar a info - done
	2) Ver o elemento de ligacao q esta no json - done
	3) Procurar esse elemento no popup 
	4) Guardar o valor desse elemento (id1) - done
	5) Ver o elemento de ligacao q esta no json da outra informacao (id2) - done
	6) Fazer pedido ajax com o id2 com o valor do id1  
	7) Tirar so a info pedida q esta no json 
	8) Inserir no popup

	_____________________________________

	1) Limpar a informacao anterior sempre que isto é chamado (guardar o id anterior)

*/
var previousId = -1;
/**** Onde ir buscar a informcacao ***/
var urlJSONFile = "./configurationFile.json";
var id_info_tag;
var id_info_attrName;
var id_info_attrValue;
var id_info_infoValue;
var id_info_param; 
var url_info;

var id_arcgis = -1;
var id_arcgis_alias = "";
var jsonFile;

var lengthElems = -1;


var test = 0;

var length_attrs = -1;
var id = "";
var map;

/*  Informacao a apresentar no popup */
var mainTags = []; // Nome da tag
var attrNames = []; // Nome do atributo caso selecionado
var attrValues = []; // Valor do atributo caso selecionado
var labels = []; // Nome das labels
var value_inLabels = []; //Caso seja o valor dentro da label
var tagInfos = []; // Tags da informacao 

var title_popup;
var table_popup;


function beforeSleep(){
 // esriPopup light esriPopupVisible
    var popup = document.getElementsByClassName('esriPopup light esriPopupVisible');
    if(popup.length > 0){
        console.log(popup[0]);
        //popup[0].style.display = 'none';
        setTimeout("update_infoWindow()",300);
    }
}

var update_infoWindow = function(){
    var popup = document.getElementsByClassName('contentPane');
    
    var sectionPopup = popup[0].getElementsByClassName('mainSection'); 
    //console.log(sectionPopup[0]);

    if(sectionPopup.length > 0){
        // colocar 'sem informação' os campos sem valor

        // Por vezes ele apaga a tabela ao inserir os elementos do ArcGiS Online
        var title_dojo = sectionPopup[0].firstChild.innerText;
        var table_numbers = dojo.query('table', sectionPopup[0]).length;
        if(table_numbers < 2 && title_popup == title_dojo){
            sectionPopup[0].append(table_popup);
            return;
        }
        var table = sectionPopup[0].getElementsByClassName('attrTable');
        //console.log(table);
        var childLength = 0;
        if(table[0] != undefined  && table[0].childNodes.length > 0){
            var tbody = table[0].firstChild;
            //console.log(tbody);
            //console.log(tbody.childNodes);
            if(tbody != null){    
                var arctags = tbody.childNodes;
                //console.log(arctags);
                var counter = arctags.length;
                if(length_attrs > 0 && length_attrs != counter){
                    return;
                }
                // Passa por todos os elementos do popup a procura do identificador
                for(var i=0; i< counter; i++) {
                    var name_label = arctags[i].childNodes[0].firstChild;
                    var val_label = arctags[i].childNodes[1].textContent;
                    //console.log(name_label.data);
                    if(name_label.data == id_arcgis_alias && id != val_label){
                        //chamar pedido ajax 
                        id = val_label;
                        connectServerjQuery(sectionPopup[0], val_label, popup, counter);   
                    }           
                    if(val_label == null || val_label == "")
                        arctags[i].childNodes[1].innerHTML = "Sem informação";                                            
                }   
            }
        }
    } 
}

function setJsonFile(jsonFile_info){
    jsonFile = jsonFile_info;
}

//Substituto do getJSON do jQuery 
var readJsonFile = function(layer_fields, info_map){
    map = info_map;
    var obj = jsonFile;
    id_info_tag = obj.id_info_tag;
    id_info_attrName = obj.id_info_attrName;
    id_info_attrValue = obj.id_info_attrValue;
    id_info_infoValue = obj.id_info_infoValue;
    id_info_param = obj.id_info_param;
    url_info = obj.url_info;
    id_arcgis = obj.id_arcgis;

    /** Informacao de cada tag extra ***/
    obj.info.forEach(function(tag, index){
        if(tag.label != undefined){
            mainTags.push(tag.tag_name);
            attrNames.push(tag.attrName);
            attrValues.push(tag.attrValue);
            value_inLabels.push(tag.value_inLabel);
            labels.push(tag.label);
            tagInfos.push(tag.value);
        }else{
        }
    });

    // Sempre que vou a procura depois so tnh a label do elemento no popup
    layer_fields.forEach(function(elem, j){ 
        if(elem.visible && (elem.fieldName == id_arcgis)){
            id_arcgis_alias = elem.label;
        }
    });    
}

var capitalizeLabel = function(string) { 
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

// Funcao que utiliza o json criado da app de configuracao
var connectServerjQuery = function(tableToInsertRow, idCode, popup, counter){
    require(["dojo/query", "dojo/request/xhr"], function(query, request){
        var table_query = query('.attrName', tableToInsertRow)[0];
        var td_width = table_query.scrollWidth;
       // console.log(map.infoWindow);
        console.log(map);
        //var table_nInfo = $('<table id="table_newInfo" class="attrTable" cellspacing="0" cellpadding="0"></table>');
        var table_nInfo = dojo.create("table", {
            class: "attrTable", 
            id: "table_newInfo", 
            cellspacing: "0", 
            cellpadding: "0"}, tableToInsertRow);
        //$(tableToInsertRow).append(table_nInfo);
        
        //var tbody_nInfo = $('<tbody id="tbody_newInfo"></tbody>');
        //table_nInfo.append(tbody_nInfo);
        var tbody_nInfo = dojo.create("tbody", {id: "tbody_newInfo"}, "table_newInfo");
        console.log('tabela criada');
        var serverUrl = url_info.split(id_info_param);
        //console.log(serverUrl);
        var idWithoutPoint = idCode.replace('.',''); // Caso os valores sejam 1.567 em vez de 1567
        idCode = idWithoutPoint;

        var realUrl = serverUrl[0] + id_info_param + "=" + idCode;
        //console.log(realUrl);
        var split_url = serverUrl[1].split(/&(.+)/);
        //console.log(split_url);
        if(split_url.length > 1){
            realUrl = realUrl + "&" + split_url[1];
        }

        //request.get(realUrl, { headers: {"X-Requested-With": null}, handleAs: "xml"}).then(
        request(realUrl, { headers: {"X-Requested-With": null}, handleAs: "xml"}).then(
        function(xmlhttp){
            var resultTag = [];
            //console.log(mainTags.length);
            var xml_tag = xmlhttp;
            //console.log(xml_tag);
            for(var k = 0; k < mainTags.length; k++){
                //console.log(mainTags[k]);
                var xml_tag_k = dojo.query(mainTags[k], xml_tag);//$(xml_tag).find(mainTags[k]);
                //console.log(xml_tag_k);
                if(attrNames[k]!=""){
                    //console.log("Associar atributo");
                    var xml_aux;
                    for(var i = 0; i < xml_tag_k.length; i++){
                        var attrs = xml_tag_k[i].attributes;
                        for(var l = 0; l < attrs.length; l++){
                            if(attrs[l].name == attrNames[k] && attrs[l].value == attrValues[k]){
                                xml_aux = xml_tag_k[i];                              
                            }
                        }
                    }
                    xml_tag_k = xml_aux;
                }
                //tratar da label 
                var label_tag;
                if(value_inLabels[k] == 'true'){
                    label_tag = dojo.query(labels[k], xml_tag_k)[0].textContent; 
                    //$(xml_tag_k).find(labels[k])[0].textContent;
                }else{
                    label_tag = labels[k];
                }
                //console.log(xml_tag_k);
                //tratar da informacao
                var xml_tag_info = tagInfos[k];
                //console.log(xml_tag_info);
                var hasChild = true;
                for(var n = 0; n < tagInfos[k].length; n++){
                    //console.log(xml_tag_info[n].tag);
                    //console.log(xml_tag_k);
                    var x_aux = dojo.query( (xml_tag_info[n].tag) , xml_tag_k);
                    //console.log(x_aux);
                    //var x_aux = $(xml_tag_k).find(xml_tag_info[n].tag);
                    if(x_aux.length == 0 && xml_tag_info[n].tag != mainTags[k]){
                        //console.log("Nao contem os filhos supostos");
                        hasChild = false;
                        break;
                    }else {
                        if(x_aux.length == 0 && xml_tag_info[n].tag == mainTags[k]){
                           // console.log('Sem filhos. E a propria label');
                            x_aux = xml_tag_k;
                        } else{
                            x_aux = x_aux[0];
                            //console.log(x_aux);
                            if(xml_tag_info[n].attrName != ""){
                                //console.log('Info com attrs associados');
                                for(var p=0; p < x_aux.length; p++){
                                    var attrs = x_aux[p].attributes;
                                    for(var s = 0; s < attrs.length; s++){
                                        if(attrs[s].name == xml_tag_info[n].attrName && attrs[s].value ==xml_tag_info[n].attrValue){
                                            xml_tag_k = x_aux[p];                              
                                        }
                                    }
                                }
                            }
                        }
                        xml_tag_k = x_aux;
                    }
                }
                var textInformation = "Sem informação";
                if(hasChild){
                    textInformation = dojo.query(xml_tag_k)[0].textContent;
                }

                var new_tr = dojo.create("tr", {valign: "top"}, tbody_nInfo);
                dojo.create("td", {
                    class: "attrName", 
                    width: td_width + "px!important;",
                    innerHTML: capitalizeLabel(label_tag)}, new_tr);
                dojo.create("td", {
                    class: "attrValue",
                    innerHTML: textInformation}, new_tr);
                if(length_attrs == -1){
                    length_attrs = counter;
                }
            }

            var newObj = new Object();
            newObj.key = idCode;
            newObj.info = dojo.query('#table_newInfo')[0];
            table_popup = table_nInfo;
            title_popup = tableToInsertRow.firstChild.innerText;
            //console.log(newObj);

           // popup[0].addEventListener("DOMSubtreeModified", beforeSleep, false);

        },
        function(error){
            console.log("An error occurred: " + error);
        }
        );
    });
}